package cz.vutbr.tam

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import cz.vutbr.tam.util.NotificationUtil
import timber.log.Timber

class AlarmReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        Timber.e("Received intent")
        NotificationUtil.recheckNotifications()
    }

}