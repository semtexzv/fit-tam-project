package cz.vutbr.tam

import android.app.AlarmManager
import android.app.Application
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.support.multidex.MultiDexApplication
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.stetho.Stetho
import com.google.android.gms.maps.MapsInitializer
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.jakewharton.threetenabp.AndroidThreeTen
import com.uphyca.stetho_realm.RealmInspectorModulesProvider
import cz.vutbr.tam.data.remote.Api
import cz.vutbr.tam.util.*
import io.reactivex.plugins.RxJavaPlugins
import io.realm.Realm
import io.realm.RealmConfiguration
import okhttp3.OkHttpClient
import org.threeten.bp.LocalDateTime
import retrofit2.Retrofit
import timber.log.Timber
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory


class App : MultiDexApplication() {




    override fun onCreate() {
        super.onCreate()
        AndroidThreeTen.init(this)
        Fresco.initialize(this)
        Timber.plant(Timber.DebugTree())
        Realm.init(this)
        MapsInitializer.initialize(this)


        instance = this

        val config = RealmConfiguration.Builder()
                .deleteRealmIfMigrationNeeded()
                .build()


        Stetho.initialize(
                Stetho.newInitializerBuilder(this)
                        .enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
                        .enableWebKitInspector(RealmInspectorModulesProvider.builder(this).build())
                        .build());

        realm = Realm.getInstance(config)
        gson = GsonBuilder()
                .create()

        httpClient = OkHttpClient()

        val retrofit = Retrofit.Builder()
                .baseUrl("http://api.event-map.eu")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()

        api = retrofit.create(Api::class.java)

        RxJavaPlugins.setErrorHandler({ Timber.e(it) })



        val manager = getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val interval = 10 * 1000L

        val alarmIntent = Intent(this, AlarmReceiver::class.java)
        val pending = PendingIntent.getBroadcast(this, 0, alarmIntent,0);
        manager.setRepeating(AlarmManager.RTC_WAKEUP,System.currentTimeMillis(),interval,pending)
    }

    companion object {

        lateinit var instance: App
            private set

        lateinit var realm: Realm
            private set

        lateinit var gson: Gson
            private set


        lateinit var httpClient: OkHttpClient
            private set

        lateinit var api: Api
            private set

        val res: Resources
            get() = instance.resources

        var compEpochTime: Long = System.currentTimeMillis()

    }
}
