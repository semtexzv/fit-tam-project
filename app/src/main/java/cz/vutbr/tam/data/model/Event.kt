package cz.vutbr.tam.data.model

import android.text.format.DateUtils
import com.google.gson.annotations.SerializedName
import cz.vutbr.tam.App
import io.realm.RealmObject
import io.realm.annotations.Ignore
import io.realm.annotations.PrimaryKey
import org.threeten.bp.LocalDateTime
import org.threeten.bp.ZoneId
import org.threeten.bp.format.DateTimeFormatter
import org.threeten.bp.format.FormatStyle
import java.time.Instant
import java.text.SimpleDateFormat
import java.util.*


open class Event : RealmObject() {

    @PrimaryKey
    var id: Int = 0

    @SerializedName("fb_id")
    var facebookId: Int? = null

    var name: String = ""
    var description: String = ""

    @SerializedName("time_start")
    var startTimeStr = ""
    @SerializedName("time_end")
    var endTimeStr = ""

    val startTime: LocalDateTime
        get() = LocalDateTime.parse(startTimeStr, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))


    val endTime: LocalDateTime
        get() = LocalDateTime.parse(endTimeStr, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))


    val startTimestampMillis: Long
        get() {
            return startTime.atZone(ZoneId.systemDefault()).toEpochSecond() * 1000
        }

    val endTimestampMillis: Long
        get() {
            return endTime.atZone(ZoneId.systemDefault()).toEpochSecond() * 1000
        }

    @SerializedName("cover_photo")
    var coverPhotoUrl: String = ""

    @SerializedName("profile_photo")
    var profilePhotoUrl: String = ""

    var type: String = ""

    @SerializedName("ppl_attending")
    var attending: Int = 0

    val attendingStr
        get() = attending.toString()

    @SerializedName("ppl_interested")
    var interested: Int = 0

    val interestedStr
        get() = interested.toString()

    var latitude: Double = 0.0
    var longitude: Double = 0.0

    var place: String = ""
    var street: String = ""
    var city: String = ""
    var zip: String = ""
    var country: String = ""

    val addressText: String
        get() = street + ", " + city

    val latLngText: String
        get() = "" + latitude + ", " + longitude

    val dateStartText: String
        get() = startTime.format(DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM))

    val dateEndText: String
        get() = endTime.format(DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM))

    val comparableVariable : Long
        get() = longMax(App.compEpochTime, startTimestampMillis)

    val itemEventDateAndType: String
        get() = getDateInfo() + ", " + type

    fun getDateInfo() : String{
        val curml = System.currentTimeMillis()
        val startml = startTimestampMillis
        val endml = endTimestampMillis

        if(curml > startml){//event already started
            if(curml > endml){//already ended
                return "ended"
            }
            else{
                val mldiff = endml - curml
                if(mldiff > 86400000){
                    val enddays = (mldiff / 86400000).toInt()
                    return enddays.toString() + " days left"
                }
                else{
                    if(mldiff < 3600000){
                        val startminutes = (mldiff / 60000).toInt()
                        if(startminutes == 1)
                            return startminutes.toString()+" minute left"
                        else
                            return startminutes.toString()+" minutes left"
                    }
                    else{
                        val endhours = (mldiff / 3600000).toInt()
                        if(endhours == 1)
                            return endhours.toString()+" hour left"
                        else
                            return endhours.toString()+" hours left"
                    }
                }
            }
        }
        else{
            val mldiff = startml - curml
            if(mldiff > 86400000){
                return startTime.format(DateTimeFormatter.ofPattern("MMM dd"))
            }
            else{
                if(mldiff < 3600000){
                    val startminutes = (mldiff / 60000).toInt()
                    if(startminutes == 1)
                        return "in "+startminutes+" minute"
                    else
                        return "in "+startminutes+" minutes"
                }
                else{
                    val starthours = (mldiff / 3600000).toInt()
                    if(starthours == 1)
                        return "in "+starthours+" hour"
                    else
                        return "in "+starthours+" hours"
                }
            }
        }


        /*
        val sval = startTime.format(DateTimeFormatter.ofPattern("MMM dd"))
        val eval = endTime.format(DateTimeFormatter.ofPattern("MMM dd"))
        if(sval.equals(eval)){
            return sval
        }
        else{
            return sval + " - " + eval
        }*/
    }

    fun longMax (a : Long, b : Long): Long {
        if(a > b)
            return a
        else
            return b
    }

    /*
     {
      "id": 464,
      "id_fb": "171568263408291",
      "name": "Oslava 6. narozenin Laser game Brno - Party",
      "description": "Přijďte to s námi pořádně rozjet na naše šesté narozeniny!\nKaždý si zahraje čtyři desetiminutové hry.\nSpousta zábavy a soutěží.\nVstupné 250 Kč pro muže, 125 Kč pro ženy.\nVstupné v prodeji od 1. listopadu.",
      "time_start": "2017-12-01 20:00:00",
      "time_end": "2017-12-01 23:59:00",
      "cover_photo": "https://scontent.xx.fbcdn.net/v/t1.0-9/s720x720/21616377_1467679736643953_4577381455621127582_n.jpg?oh=4e1f2f7cfeeb2130ed1da43ef63f3757&oe=5A7AEBF0",
      "profile_photo": "https://scontent.xx.fbcdn.net/v/t1.0-0/c50.0.200.200/p200x200/21616377_1467679736643953_4577381455621127582_n.jpg?oh=697d7ac8bc9da18bad28762aeefe27c2&oe=5A6C3961",
      "type": "Laser Tag Center",
      "ppl_attending": 3,
      "ppl_interested": 38,
      "latitude": "49.1901900",
      "longitude": "16.5995600",
      "place": "Laser Game Brno",
      "street": "Hybešova 46",
      "city": "Brno",
      "zip": "60200",
      "country": "Czech Republic"
    },
     */

}