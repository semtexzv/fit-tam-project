package cz.vutbr.tam.data.model

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class EventNotification
@JvmOverloads
constructor(_event: Event? = null) : RealmObject() {

    // Setters to these fields are private, so they can't be modified by user, and are set on creation of the object
    @PrimaryKey
    var id = _event?.id ?: 0
        private set

    var event = _event
        private set


    var selectedOptionIndex : Int = 0
}