package cz.vutbr.tam.data.remote

import com.google.gson.JsonObject
import cz.vutbr.tam.data.model.Event
import cz.vutbr.tam.data.remote.types.CallResult
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.threeten.bp.LocalDateTime
import org.threeten.bp.format.DateTimeFormatter
import retrofit2.http.GET
import retrofit2.http.Query
import java.util.*

interface Api {
    @GET("get_events.php")
    fun _getEvents(@Query("pos_latitude") pos_latitude: Double,
                   @Query("pos_longitude") pos_longitude: Double,
                   @Query("date_from") date_from: String,
                   @Query("date_to") date_to: String): Single<CallResult<List<Event>>>
}

fun Api.getEvents(lat: Double, lon: Double,
                  since: LocalDateTime = LocalDateTime.now(),
                  until: LocalDateTime = LocalDateTime.now().plusMonths(24)): Single<List<Event>> {

    return _getEvents(lat, lon, since.format(DateTimeFormatter.ISO_DATE), until.format(DateTimeFormatter.ISO_DATE))
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .map { it.data!! }
}
