package cz.vutbr.tam.data.remote.types

import com.google.gson.JsonObject

class CallResult<T> {
    var header : JsonObject? = null
    var meta : JsonObject? = null
    var data : T? = null
}