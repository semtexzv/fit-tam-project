package cz.vutbr.tam.ui.base

import android.content.Context
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.annotation.*
import android.support.v7.app.AppCompatActivity
import cz.vutbr.tam.BR
import cz.vutbr.tam.App
import cz.vutbr.tam.ui.base.view.MvvmView
import cz.vutbr.tam.ui.base.viewmodel.MvvmViewModel
import cz.vutbr.tam.util.ContextProvider

abstract class BaseActivity<B : ViewDataBinding, VM : MvvmViewModel<*>> : AppCompatActivity(), MvvmView, ContextProvider {

    protected lateinit var binding: B
    protected lateinit var viewModel: VM

    @CallSuper
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        viewModel.saveInstanceState(outState)
    }

    @CallSuper
    override fun onDestroy() {
        super.onDestroy()
        viewModel.detachView()
    }

    protected fun initView(savedInstanceState: Bundle?, @LayoutRes layoutResID: Int) {
        binding = DataBindingUtil.setContentView<B>(this, layoutResID)
        binding.setVariable(BR.vm, viewModel)

        (viewModel as MvvmViewModel<MvvmView>).attachView(this, savedInstanceState)
    }

    override val context: Context?
        get() = this

}
