package cz.vutbr.tam.ui.base

import android.content.Context
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.annotation.*
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import cz.vutbr.tam.BR
import cz.vutbr.tam.ui.base.view.MvvmView
import cz.vutbr.tam.ui.base.viewmodel.MvvmViewModel

open class BaseDialogFragment<B : ViewDataBinding, VM : MvvmViewModel<*>> : DialogFragment(), MvvmView {

    protected lateinit var binding: B
    protected lateinit var viewModel: VM


    @CallSuper
    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        viewModel.saveInstanceState(outState)
    }

    @CallSuper
    override fun onDestroyView() {
        super.onDestroyView()
        viewModel.detachView()

    }

    @CallSuper
    override fun onDestroy() {
        super.onDestroy()
    }


    /* Sets the content view, creates the binding and attaches the view to the view model */
    protected fun setAndBindContentView(savedInstanceState: Bundle?, @LayoutRes layoutResID: Int): View {
        binding = DataBindingUtil.inflate<B>(LayoutInflater.from(context), layoutResID, null, false)
        binding.setVariable(BR.vm, viewModel)
        @Suppress("UNCHECKED_CAST")
        (viewModel as MvvmViewModel<MvvmView>).attachView(this, savedInstanceState)

        return binding.root
    }

}
