package cz.vutbr.tam.ui.base

import android.content.Context
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.support.v7.widget.RecyclerView
import android.view.View

import cz.vutbr.tam.BR
import cz.vutbr.tam.ui.base.view.MvvmView
import cz.vutbr.tam.ui.base.viewmodel.MvvmViewModel
import cz.vutbr.tam.util.ContextProvider

abstract class BaseViewHolder<B : ViewDataBinding, VM : MvvmViewModel<*>>(itemView: View) : RecyclerView.ViewHolder(itemView), MvvmView, ContextProvider {

    protected lateinit var binding: B
    lateinit var viewModel: VM


    protected fun initView(view: View) {
        binding = DataBindingUtil.bind(view)
        binding.setVariable(BR.vm, viewModel)

        (viewModel as MvvmViewModel<MvvmView>).attachView(this, null)
    }

    override val context: Context?
        get() = itemView?.context

}
