package cz.vutbr.tam.ui.base.viewmodel

import android.databinding.BaseObservable
import android.os.Bundle
import android.support.annotation.CallSuper

import cz.vutbr.tam.ui.base.view.MvvmView


abstract class BaseViewModel<V : MvvmView> : BaseObservable(), MvvmViewModel<V> {

    var view: V? = null
        private set

    @CallSuper
    override fun attachView(view: V, savedInstanceState: Bundle?) {
        this.view = view
        if (savedInstanceState != null) { restoreInstanceState(savedInstanceState) }
    }

    @CallSuper
    override fun detachView() {
        view = null
    }

    protected open fun restoreInstanceState(savedInstanceState: Bundle) { }

    override fun saveInstanceState(outState: Bundle?) { }

    val isViewAttached: Boolean
        get() = view != null

    fun checkViewAttached() {
        if (!isViewAttached) throw RuntimeException("View is not attached")
    }
}
