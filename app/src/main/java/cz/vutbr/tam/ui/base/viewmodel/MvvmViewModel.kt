package cz.vutbr.tam.ui.base.viewmodel

import android.databinding.Observable
import android.os.Bundle

import cz.vutbr.tam.ui.base.view.MvvmView

interface MvvmViewModel<V : MvvmView> : Observable {
    fun attachView(view: V, savedInstanceState: Bundle?)
    fun detachView()

    fun saveInstanceState(outState: Bundle?)
}
