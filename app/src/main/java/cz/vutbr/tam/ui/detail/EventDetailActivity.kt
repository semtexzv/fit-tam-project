package cz.vutbr.tam.ui.detail

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import cz.vutbr.tam.R
import cz.vutbr.tam.databinding.ActivityEventDetailBinding
import cz.vutbr.tam.ui.base.BaseActivity
import cz.vutbr.tam.util.getArg

class EventDetailActivity : BaseActivity<ActivityEventDetailBinding, EventDetailViewModel>() {

    init {
        viewModel = EventDetailViewModel()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView(savedInstanceState, R.layout.activity_event_detail)

        setSupportActionBar(binding.toolbar)
        supportActionBar?.title = "Event detail"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        val id = getArg<Int>()
        if(id != null){
            viewModel.loadData(id)
        }
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> {
                super.onOptionsItemSelected(item)

            }
        }
    }
}
