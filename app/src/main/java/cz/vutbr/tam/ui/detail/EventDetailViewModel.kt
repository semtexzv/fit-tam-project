package cz.vutbr.tam.ui.detail

import android.databinding.Bindable
import android.graphics.drawable.Drawable
import android.support.v4.graphics.drawable.DrawableCompat
import android.view.View
import cz.vutbr.tam.App
import cz.vutbr.tam.BR
import cz.vutbr.tam.R
import cz.vutbr.tam.data.model.Event
import cz.vutbr.tam.ui.base.viewmodel.BaseViewModel
import cz.vutbr.tam.util.NotifyPropChanged
import android.content.Intent
import cz.vutbr.tam.data.model.EventNotification
import cz.vutbr.tam.ui.main.MainActivity
import cz.vutbr.tam.ui.notify.NotifyDialogFragment
import cz.vutbr.tam.util.NotificationUtil
import cz.vutbr.tam.util.startActivityWithArg


class EventDetailViewModel : BaseViewModel<EventDetailActivity>() {

    @get:Bindable
    var event: Event? by NotifyPropChanged(null, BR.event)

    @get:Bindable
    var eventNotif: EventNotification? by NotifyPropChanged(null, BR.eventNotif, BR.subscribed, BR.alarmIcon)


    @get:Bindable
    val subscribed: Boolean
        get() = eventNotif?.selectedOptionIndex ?: 0 != 0


    @get:Bindable
    val alarmIcon: Drawable
        get() {
            if (subscribed) {
                val d = App.res.getDrawable(R.drawable.alarm_check)
                DrawableCompat.setTint(d, App.res.getColor(R.color.colorPrimary))
                return d
            } else {

                val d = App.res.getDrawable(R.drawable.alarm_off)
                DrawableCompat.setTint(d, App.res.getColor(R.color.appGray))
                return d
            }
        }


    val onMapIconClickListener = View.OnClickListener {
        val id = event?.id
        view?.startActivityWithArg(MainActivity::class.java, id, Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP)
    }

    val onAlarmIconClickListener = View.OnClickListener {
        val frag = NotifyDialogFragment(event!!, eventNotif?.selectedOptionIndex ?: 0) { notif ->
            App.realm.executeTransaction { it.insertOrUpdate(notif) }
            eventNotif = notif
            NotificationUtil.recheckNotifications()
        }
        frag.show(view?.supportFragmentManager, "Notify")

    }

    fun loadData(id: Int) {
        event = App.realm.where(Event::class.java).equalTo("id", id).findFirst()
        eventNotif = App.realm.where(EventNotification::class.java).equalTo("id", event?.id!!).findFirst()
    }


}