package cz.vutbr.tam.ui.filter

import cz.vutbr.tam.data.model.Event
import cz.vutbr.tam.util.MILLISECONDS_PER_DAY
import cz.vutbr.tam.util.MILLISECONDS_PER_MONTH
import cz.vutbr.tam.util.MILLISECONDS_PER_WEEK

// This class will represent selected event filter, and will be applied, along with search information
enum class EventTimeFilter(val text: String, val startOffsetMillis: Long) {

    Ongoing("Ongoing", 0),
    Today("Today", MILLISECONDS_PER_DAY),
    ThisWeek("Next 7 days", MILLISECONDS_PER_WEEK),
    ThisMonth("Next 30 days", MILLISECONDS_PER_MONTH),
    AllTime("All time", Long.MAX_VALUE);

    fun eventMatches(ev: Event): Boolean {
        return ev.startTimestampMillis - System.currentTimeMillis() < this.startOffsetMillis
    }


}