package cz.vutbr.tam.ui.filter

import android.app.Dialog
import android.os.Bundle
import com.afollestad.materialdialogs.MaterialDialog
import cz.vutbr.tam.databinding.DialogFilterBinding
import cz.vutbr.tam.ui.base.BaseDialogFragment

class FilterDialogFragment(val old: EventTimeFilter, val cb: ((EventTimeFilter) -> Unit)) : BaseDialogFragment<DialogFilterBinding, FilterDialogViewModel>() {

    init {
        viewModel = FilterDialogViewModel()
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return MaterialDialog.Builder(context)
                .title("Filter Events")
                .items(EventTimeFilter.values().map { it.text })
                .itemsCallbackSingleChoice(old.ordinal) { a, b, c, d ->
                    true
                }
                .positiveText("Apply")
                .negativeText("Cancel")
                .onPositive { materialDialog, dialogAction ->
                    val filter = if (materialDialog.selectedIndex < 0 || materialDialog.selectedIndex >= EventTimeFilter.values().size) {
                        EventTimeFilter.AllTime
                    } else {
                        EventTimeFilter.values()[materialDialog.selectedIndex]
                    }
                    cb(filter)
                }
                .build()
    }
}