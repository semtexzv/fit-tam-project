package cz.vutbr.tam.ui.list

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.view.MenuItemCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.view.Menu
import android.view.MenuItem
import cz.vutbr.tam.App
import cz.vutbr.tam.R
import cz.vutbr.tam.databinding.ActivityEventListBinding
import cz.vutbr.tam.ui.base.BaseActivity
import cz.vutbr.tam.ui.filter.EventTimeFilter
import cz.vutbr.tam.ui.filter.FilterDialogFragment
import timber.log.Timber

class EventListActivity : BaseActivity<ActivityEventListBinding, EventListViewModel>() {

    init {
        App.compEpochTime = System.currentTimeMillis()
        viewModel = EventListViewModel()
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView(savedInstanceState, R.layout.activity_event_list)
        setSupportActionBar(binding.toolbar)
        supportActionBar?.title = "Event List"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        binding.recycler.layoutManager = LinearLayoutManager(this)

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.event_list, menu)

        val search = menu!!.findItem(R.id.search)!!

        val searchView = search.actionView as SearchView
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(p0: String?): Boolean {
                viewModel.searchTextChanged(p0 ?: "");
                return true;
            }

            override fun onQueryTextChange(p0: String?): Boolean {
                viewModel.searchTextChanged(p0 ?: "");
                return true
            }
        });

        MenuItemCompat.setOnActionExpandListener(search, object : MenuItemCompat.OnActionExpandListener {
            override fun onMenuItemActionExpand(p0: MenuItem?): Boolean {
                menu!!.findItem(R.id.filter)!!.setVisible(false)
                supportActionBar?.setDisplayHomeAsUpEnabled(false)
                Timber.e("Opened")
                return true
            }

            override fun onMenuItemActionCollapse(p0: MenuItem?): Boolean {
                menu!!.findItem(R.id.filter)!!.setVisible(true)
                supportActionBar?.setDisplayHomeAsUpEnabled(true)
                Timber.e("closed")
                return true
            }
        })
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            R.id.filter -> {
                FilterDialogFragment(viewModel.filter, {
                    viewModel.filterChanged(it)
                    // TOdo, apply icons
                    if(it == EventTimeFilter.AllTime) {

                    } else {

                    }
                }).show(supportFragmentManager, "Filter")
                true
            }
            else -> {
                super.onOptionsItemSelected(item)

            }
        }
    }
}
