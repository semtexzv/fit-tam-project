package cz.vutbr.tam.ui.list

import android.databinding.Bindable
import com.android.databinding.library.baseAdapters.BR
import cz.vutbr.tam.App
import cz.vutbr.tam.data.model.Event
import cz.vutbr.tam.ui.base.viewmodel.BaseViewModel
import cz.vutbr.tam.ui.filter.EventTimeFilter
import cz.vutbr.tam.ui.list.recycler.EventListAdapter
import cz.vutbr.tam.util.NotifyPropChanged
import cz.vutbr.tam.util.Search

class EventListViewModel : BaseViewModel<EventListActivity>() {

    val events = App.realm.where(Event::class.java).findAll().sortedWith(compareBy({ it.comparableVariable }, { it.endTimestampMillis }))

    @get:Bindable
    var searchedEvents: List<Event> by NotifyPropChanged(events, BR.adapter, BR.searchedEvents)

    @get:Bindable
    var filteredEvents: List<Event> by NotifyPropChanged(events, BR.adapter, BR.filteredEvents)

    @get:Bindable
    var adapter = EventListAdapter(searchedEvents)

    @get:Bindable
    var filter by NotifyPropChanged(EventTimeFilter.AllTime, BR.filter, BR.adapter)

    @get:Bindable
    var searchQuery by NotifyPropChanged("", BR.searchQuery)

    fun reloadItems() {
        filteredEvents = Search.getEventsForFilter(events, filter)
        searchedEvents = Search.getEventsForQuery(filteredEvents, searchQuery)
        adapter = EventListAdapter(searchedEvents)
    }

    fun searchTextChanged(s: String) {
        searchQuery = s;
        reloadItems()

    }

    fun filterChanged(f: EventTimeFilter) {
        filter = f;
        reloadItems()
    }

}