package cz.vutbr.tam.ui.list.recycler

import android.content.Intent
import android.view.View
import cz.vutbr.tam.data.model.Event
import cz.vutbr.tam.databinding.ItemEventBinding
import cz.vutbr.tam.ui.base.BaseViewHolder
import cz.vutbr.tam.ui.detail.EventDetailActivity
import cz.vutbr.tam.util.startActivityWithArg

class EventItemViewHolder(view: View) : BaseViewHolder<ItemEventBinding, EventItemViewModel>(view) {
    init {
        viewModel = EventItemViewModel()
        initView(view)
    }

    fun startDetailActivity() {
        itemView?.context?.startActivityWithArg(EventDetailActivity::class.java, viewModel?.event?.id)
    }

    fun setEvent(ev: Event?) {
        viewModel.event = ev
    }


}