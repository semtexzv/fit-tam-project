package cz.vutbr.tam.ui.list.recycler

import android.content.Intent
import android.databinding.Bindable
import android.view.View
import cz.vutbr.tam.BR
import cz.vutbr.tam.data.model.Event
import cz.vutbr.tam.ui.base.viewmodel.BaseViewModel
import cz.vutbr.tam.ui.detail.EventDetailActivity
import cz.vutbr.tam.util.NotifyPropChanged
import cz.vutbr.tam.util.activity
import cz.vutbr.tam.util.context

class EventItemViewModel : BaseViewModel<EventItemViewHolder>() {


    @get:Bindable
    var event: Event? by NotifyPropChanged(null, BR.event)

    @get:Bindable
    var onItemClickListener = View.OnClickListener {
        view?.startDetailActivity()
    }
}