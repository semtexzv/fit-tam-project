package cz.vutbr.tam.ui.list.recycler

import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import cz.vutbr.tam.R
import cz.vutbr.tam.data.model.Event
import cz.vutbr.tam.util.inflateViewHolder

class EventListAdapter(val events: List<Event>?) : RecyclerView.Adapter<EventItemViewHolder>() {

    var decor : DividerItemDecoration? = null
    override fun onAttachedToRecyclerView(recyclerView: RecyclerView?) {
        super.onAttachedToRecyclerView(recyclerView)
        decor = DividerItemDecoration(recyclerView?.context,DividerItemDecoration.VERTICAL)
        recyclerView?.addItemDecoration(decor)
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView?) {
        super.onDetachedFromRecyclerView(recyclerView)
        recyclerView?.removeItemDecoration(decor)
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): EventItemViewHolder {
        return inflateViewHolder(p0, R.layout.item_event, { EventItemViewHolder(it) })
    }

    override fun onBindViewHolder(p0: EventItemViewHolder?, p1: Int) {
        p0?.setEvent(events?.get(p1))
    }

    override fun getItemCount(): Int {
        return events?.size ?: 0
    }

}