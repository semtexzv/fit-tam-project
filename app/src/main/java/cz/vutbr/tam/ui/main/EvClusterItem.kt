package cz.vutbr.tam.ui.main

import android.text.format.DateUtils
import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.clustering.ClusterItem
import cz.vutbr.tam.data.model.Event


class EvClusterItem(val event: Event, val indexPerLocation: Int, val countPerLocation: Int) : ClusterItem {
    companion object {
        val BASE_CIRCLE_DIST = 0.55f;
        val CIRCLE_DIST_OFFSET = 0.5f
        val MARKERS_PER_CIRCLE = 10;
    }

    //val pos = LatLng(event.latitude + Math.random() * 0.0001f, event.longitude + Math.random() * 0.0001f)
    val pos = moveInAngle(event, indexPerLocation)
    val startTime = event.startTimestampMillis
    val _title = event.name
    val ev_id = event.id

    override fun getSnippet(): String {
        return "Start: " + DateUtils.getRelativeTimeSpanString(startTime, System.currentTimeMillis(), DateUtils.MINUTE_IN_MILLIS).toString();
    }

    override fun getTitle(): String = _title

    override fun getPosition(): LatLng = pos

    private fun moveInAngle(event: Event, nth: Int): LatLng {
        if (nth == 0) {
            return LatLng(event.latitude, event.longitude)
        } else {
            val count = minOf(countPerLocation, MARKERS_PER_CIRCLE)
            var angle = (360 / (count - 1)) * indexPerLocation
            var distancemMult = BASE_CIRCLE_DIST
            while (angle > 360) {
                distancemMult += CIRCLE_DIST_OFFSET
                angle -= 360
            }
            val posx = event.latitude + Math.sin(Math.toRadians(angle.toDouble())) * (distancemMult * 0.00007f)
            val posy = event.longitude + Math.cos(Math.toRadians(angle.toDouble())) * (distancemMult * 0.0001f)
            return LatLng(posx, posy)
        }
    }

}