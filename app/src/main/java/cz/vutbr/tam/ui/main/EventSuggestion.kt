package cz.vutbr.tam.ui.main

import android.os.Parcelable
import com.arlib.floatingsearchview.suggestions.model.SearchSuggestion
import cz.vutbr.tam.data.model.Event
import org.parceler.Parcel
import org.parceler.ParcelClass

class EventSuggestion(val id: Int, val name: String) : SearchSuggestion {

    override fun getBody(): String {
        return name
    }


    constructor(parcel: android.os.Parcel) : this(parcel.readInt(),parcel.readString()) {
    }

    override fun writeToParcel(parcel: android.os.Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(name)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<EventSuggestion> {
        override fun createFromParcel(parcel: android.os.Parcel): EventSuggestion {
            return EventSuggestion(parcel)
        }

        override fun newArray(size: Int): Array<EventSuggestion?> {
            return arrayOfNulls(size)
        }
    }

}