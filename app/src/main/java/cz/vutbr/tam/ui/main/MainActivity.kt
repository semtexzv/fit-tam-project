package cz.vutbr.tam.ui.main

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.graphics.Color
import android.location.Location
import android.os.Bundle
import android.support.graphics.drawable.ArgbEvaluator
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v4.graphics.ColorUtils
import com.arlib.floatingsearchview.FloatingSearchView
import com.arlib.floatingsearchview.suggestions.model.SearchSuggestion
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.*
import com.google.maps.android.clustering.ClusterManager
import com.google.maps.android.clustering.view.ClusterRenderer
import com.google.maps.android.clustering.view.DefaultClusterRenderer
import com.uchuhimo.collections.mutableBiMapOf
import cz.vutbr.tam.BR
import cz.vutbr.tam.R
import cz.vutbr.tam.databinding.ActivityMainBinding
import cz.vutbr.tam.ui.base.BaseActivity
import cz.vutbr.tam.ui.detail.EventDetailActivity
import cz.vutbr.tam.ui.filter.FilterDialogFragment
import cz.vutbr.tam.util.*
import org.threeten.bp.Duration
import org.threeten.bp.LocalDateTime
import timber.log.Timber


class MainActivity : BaseActivity<ActivityMainBinding, MainViewModel>(), ClusterManager.OnClusterItemInfoWindowClickListener<EvClusterItem> {



    init {
        viewModel = MainViewModel()
    }

    var markers = mutableBiMapOf<EvClusterItem, Int>()

    var zoomToMarkerId = -1

    override fun onClusterItemInfoWindowClick(p0: EvClusterItem?) {
        val id = p0?.ev_id


        if (id != null) {
            startActivityWithArg(EventDetailActivity::class.java, id)
        }
    }


    fun updateMarkers() {
        markers.clear()
        gmap?.clear()
        clusterManager.clearItems()

        val samePosTable = mutableMapOf<LatLng, Int>()
        val grouped = viewModel.filteredEvents.groupBy { LatLng(it.latitude, it.longitude) }

        for ((pos, evlist) in grouped) {
            evlist.forEachIndexed { index, event ->
                val item = EvClusterItem(event, index, evlist.count());
                clusterManager.addItem(item)
                markers.put(item, event.id)
            }

        }

        clusterManager.cluster()

        val poly = PolylineOptions()

                .add(LatLng(49.146696, 16.54530))
                .add(LatLng(49.146696, 16.664474))
                .add(LatLng(49.24455, 16.664474))
                .add(LatLng(49.24455, 16.54530))
                .add(LatLng(49.146696, 16.54530))
                .color(ColorUtils.setAlphaComponent(resources.getColor(R.color.colorPrimary), 100))
                .visible(true)
        gmap?.addPolyline(poly);

    }

    fun moveCameraToLocation(loc: Location?, zoom: Float = (gmap?.cameraPosition?.zoom ?: 12.0f)) {
        gmap?.animateCamera(CameraUpdateFactory.newLatLngZoom(
                LatLng(loc?.latitude ?: 0.0, loc?.longitude ?: 0.0), zoom
        ))
    }

    lateinit var locationProvider: FusedLocationProviderClient
    val locationCallback = object : LocationCallback() {
        override fun onLocationResult(loc: LocationResult?) {
            viewModel.updateData(loc?.lastLocation!!)
            if (viewModel.followMe) {
                moveCameraToLocation(loc?.lastLocation, Math.max(gmap?.cameraPosition?.zoom ?: 12f, 12f))
            }
            super.onLocationResult(loc)
        }
    }


    val vmPropChangedCallback = propChangedCallback {
        if (it == BR.followMe) {
            if (viewModel.followMe) {
                val req = LocationRequest.create()
                locationProvider.requestLocationUpdates(req, locationCallback, mainLooper)
            } else {
                locationProvider.removeLocationUpdates(locationCallback)
            }
        } else if (it == BR.events || it == BR.filter) {
            updateMarkers()
        }
    }

    var gmap: GoogleMap? = null
    lateinit var clusterManager: ClusterManager<EvClusterItem>
    lateinit var renderer: ClusterRenderer<EvClusterItem>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        locationProvider = FusedLocationProviderClient(this)

        initView(savedInstanceState, R.layout.activity_main)
        viewModel.addOnPropertyChangedCallback(vmPropChangedCallback)
        initBrno()

        binding.map.onCreate(null)
        binding.map.getMapAsync { map ->
            gmap = map

            /*
49.146696 16.54530
49.146696 16.664474
49.24455 16.664474
49.24455 16.54530
             */
            gmap?.mapType = GoogleMap.MAP_TYPE_NORMAL
            checkPermissions()
            gmap?.moveCamera(CameraUpdateFactory.newLatLngZoom(
                    LatLng(49.1951, 16.6068), 12.0f
            ))
            clusterManager = ClusterManager<EvClusterItem>(this, gmap)
            gmap?.uiSettings?.setMapToolbarEnabled(false)
            map?.setOnCameraIdleListener(clusterManager)
            map?.setOnMarkerClickListener(clusterManager)
            map?.setOnInfoWindowClickListener(clusterManager)
            map?.setLatLngBoundsForCameraTarget(LatLngBounds(
                    LatLng(49.0, 16.3),
                    LatLng(49.5, 16.9)
            ))
            map?.setMinZoomPreference(10f)
            map?.uiSettings?.isTiltGesturesEnabled = false

            renderer = object : DefaultClusterRenderer<EvClusterItem>(this, gmap, clusterManager) {

                override fun onBeforeClusterItemRendered(item: EvClusterItem, markerOptions: MarkerOptions) {
                    val bm = BitmapFactory.decodeResource(resources, R.drawable.marker)
                    val d = (item.startTime - System.currentTimeMillis()) / (MILLISECONDS_PER_DAY)
                    val desc = DataUtil.icons[minOf(DataUtil.icons.size - 1, maxOf(0, d.toInt()))]

                    markerOptions.icon(desc);

                    super.onBeforeClusterItemRendered(item, markerOptions)
                }

                override fun onClusterItemRendered(clusterItem: EvClusterItem?, marker: Marker?) {
                    val marker_id = clusterItem?.ev_id
                    if (marker_id == zoomToMarkerId) {
                        marker?.showInfoWindow()
                        zoomToMarkerId = -1
                    }
                    super.onClusterItemRendered(clusterItem, marker)
                }
            }
            clusterManager.renderer = renderer
            clusterManager.setOnClusterItemInfoWindowClickListener(this)

            clusterManager.setOnClusterItemInfoWindowClickListener(this)
            clusterManager.setOnClusterClickListener {
                val builder = LatLngBounds.builder();
                for (x in it.items) {
                    builder.include(x.pos)
                }
                gmap?.animateCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 100))
                true
            }


            updateMarkers()

        }
        binding.search.setOnMenuItemClickListener {
            FilterDialogFragment(viewModel.filter, {
                viewModel.filter = it
            }).show(supportFragmentManager!!, "Filter")
        }
        binding.search.setOnQueryChangeListener { old, new ->
            binding.search.swapSuggestions(
                    Search.getEventsForQuery(viewModel.events!!, new).map { EventSuggestion(it.id, it.name) })
        }
        binding.search.setOnSearchListener(object : FloatingSearchView.OnSearchListener {
            override fun onSuggestionClicked(p0: SearchSuggestion?) {
                val s = p0 as EventSuggestion
                binding.search.clearQuery()
                binding.search.clearSearchFocus()
                focusEvent(s.id)
            }

            override fun onSearchAction(p0: String?) {

            }
        })
        //checkPermissions()
        onNewIntent(intent)
    }

    fun focusEvent(id: Int, animate: Boolean = true) {
        for (ev in viewModel.events) {
            if (ev.id == id) {
                val item = markers.inverse.get(id)
                val marker: Marker? = (clusterManager.renderer as DefaultClusterRenderer<EvClusterItem>).getMarker(item)
                if (marker != null) {
                    marker.showInfoWindow()
                } else {
                    zoomToMarkerId = ev.id
                }
                if (animate) {
                    gmap?.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(ev.latitude, ev.longitude), 20f))
                } else {
                    gmap?.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(ev.latitude, ev.longitude), 20f))
                }
            }
        }
    }

    fun initBrno() {
        val loc: Location = Location("")
        loc.latitude = 49.198273
        loc.longitude = 16.606301
        viewModel.updateData(loc)
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        val id = getArg<Int>(intent)
        if (id != null && markers.isNotEmpty()) {
            focusEvent(id, false)
        }
    }

    private fun checkPermissions() {
        if (!hasPermission(Manifest.permission.ACCESS_FINE_LOCATION)) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 666);
        } else {
            gmap?.isMyLocationEnabled = true

        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 666) {
            checkPermissions()
        }
    }

    override fun onStart() {
        super.onStart()
        binding.map.onStart()
    }

    override fun onResume() {
        super.onResume()
        binding.map.onResume()
    }

    override fun onPause() {
        binding.map.onPause()
        super.onPause()
    }

    override fun onStop() {
        super.onStop()
        binding.map.onStop()
    }

    override fun onDestroy() {
        binding.map.onDestroy()
        viewModel.removeOnPropertyChangedCallback(vmPropChangedCallback)
        super.onDestroy()

    }
}
