package cz.vutbr.tam.ui.main

import android.content.Intent
import android.databinding.Bindable
import android.location.Location
import android.view.View
import cz.vutbr.tam.App
import cz.vutbr.tam.BR
import cz.vutbr.tam.R
import cz.vutbr.tam.data.model.Event
import cz.vutbr.tam.ui.base.viewmodel.BaseViewModel
import cz.vutbr.tam.ui.filter.EventTimeFilter
import cz.vutbr.tam.ui.list.EventListActivity
import cz.vutbr.tam.util.DataUtil
import cz.vutbr.tam.util.NotifyPropChanged
import cz.vutbr.tam.util.Search
import timber.log.Timber

class MainViewModel : BaseViewModel<MainActivity>() {


    @get:Bindable
    var followMe: Boolean by NotifyPropChanged(false, BR.followMe, BR.myPositionFabColor)

    @get:Bindable
    var events: List<Event> by NotifyPropChanged(App.realm.where(Event::class.java).findAll(), BR.events, BR.filteredEvents)

    @get:Bindable
    val filteredEvents: List<Event>
        get() {
            return Search.getEventsForFilter(events, filter)

        }

    @get:Bindable
    var filter: EventTimeFilter by NotifyPropChanged(EventTimeFilter.AllTime, BR.filter, BR.filteredEvents)


    fun updateData(loc: Location) {

        DataUtil.updateEvents(loc.latitude!!, loc.longitude!!)
                .doOnSuccess {
                    events = it

                }
                .subscribe()


    }

    @get:Bindable
    val myPositionFabColor: Int
        get() {
            return if (followMe) {
                App.res.getColor(R.color.colorPrimary)
            } else {
                App.res.getColor(R.color.appGray)
            }

        }


    @get:Bindable
    val onMenuFabClickListener = View.OnClickListener {
        val i = Intent(view, EventListActivity::class.java)
        view?.startActivity(i)
    }

    @get:Bindable
    val onMyPositionClickListener = View.OnClickListener {
        Timber.e("Called")
        followMe = !followMe
    }


    /*
    @get:Bindable
    val onFilterIconClickListener = View.OnClickListener {
        FilterDialogFragment().show(view?.supportFragmentManager!!, "Filter")
    }
    */
}
