package cz.vutbr.tam.ui.notify

import android.app.Dialog
import android.os.Bundle
import com.afollestad.materialdialogs.MaterialDialog
import cz.vutbr.tam.data.model.Event
import cz.vutbr.tam.data.model.EventNotification
import cz.vutbr.tam.databinding.ItemEventBinding
import cz.vutbr.tam.ui.base.BaseDialogFragment
import cz.vutbr.tam.ui.base.viewmodel.BaseViewModel
import cz.vutbr.tam.util.NotificationUtil

class NotifyDialogViewModel : BaseViewModel<NotifyDialogFragment>() {

}

class NotifyDialogFragment(val event: Event, val oldIndex: Int = 0, val cb: ((EventNotification) -> Unit)) : BaseDialogFragment<ItemEventBinding, NotifyDialogViewModel>() {



    init {
        viewModel = NotifyDialogViewModel()
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return MaterialDialog.Builder(context)
                .items(NotificationUtil.OptionTexts)
                .title("Event Notification")
                .positiveText("Confirm")
                .negativeText("Cancel")
                .onPositive { materialDialog, dialogAction ->
                    val notif = EventNotification(event)
                    notif.selectedOptionIndex = materialDialog.selectedIndex
                    cb(notif)
                }
                .itemsCallbackSingleChoice(oldIndex, { dialog, view, which, text ->
                    true
                })
                .show()
    }
}