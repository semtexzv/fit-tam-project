package cz.vutbr.tam.ui.splash

import android.Manifest
import android.app.Activity
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.ActivityCompat
import cz.vutbr.tam.R
import cz.vutbr.tam.ui.main.MainActivity
import cz.vutbr.tam.util.DataUtil
import cz.vutbr.tam.util.hasPermission
import cz.vutbr.tam.util.startActivityWithArg
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class SplashActivity : Activity() {

    private fun checkPermissions() {
        if (!hasPermission(Manifest.permission.ACCESS_FINE_LOCATION)) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 666);
        } else {
            loadIconsAndStart()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 666) {
            checkPermissions()
        }
    }

    fun loadIconsAndStart() {
        Single.fromCallable {
            DataUtil.createIcons(resources)
        }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .doOnSuccess {
                    DataUtil.deleteOldEvents()
                    startActivityWithArg(MainActivity::class.java, null)
                }
                .subscribe()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        checkPermissions()

        /*
        Handler().postDelayed({
            startActivityWithArg(MainActivity::class.java, null)
        }, 2000)
        */

    }
}