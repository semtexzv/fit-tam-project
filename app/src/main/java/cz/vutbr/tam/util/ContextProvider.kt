package cz.vutbr.tam.util

import android.content.Context

interface ContextProvider {
    val context : Context?
}