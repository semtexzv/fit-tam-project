package cz.vutbr.tam.util

import android.content.res.Resources
import android.graphics.BitmapFactory
import android.graphics.Color
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import cz.vutbr.tam.App
import cz.vutbr.tam.R
import cz.vutbr.tam.data.model.Event
import cz.vutbr.tam.data.remote.getEvents
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.threeten.bp.LocalDateTime
import timber.log.Timber

object DataUtil {

    fun deleteOldEvents()  {
        App.realm.executeTransaction { realm ->
            val all = App.realm.where(Event::class.java).findAll();
            for (it in all) {
                Timber.e("DELETING ${it.name} , already happened")
                if (it.endTime.isBefore(LocalDateTime.now())) {
                    it.deleteFromRealm()
                }
            }
        }
        App.realm.refresh()
    }
    fun updateEvents(lat: Double, lon: Double): Single<List<Event>> {
        return App.api.getEvents(lat, lon)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .map { data ->
                    App.realm.executeTransaction {
                        it.insertOrUpdate(data)
                    }
                    deleteOldEvents()
                    App.realm.where(Event::class.java).findAll()
                }
    }

    var icons: List<BitmapDescriptor> = listOf()

    fun createIcons(resources: Resources) {
        val density = resources.displayMetrics.density
        val iconSrc = BitmapFactory.decodeResource(resources, R.drawable.marker).resize((48 * density).toInt(), (48 * density).toInt())
        val eval = android.animation.ArgbEvaluator()
        icons = (0..100).map { it.toFloat() }
                .map {
                    iconSrc.tint(eval.evaluate(it / 100f, resources.getColor(R.color.markerGreen), resources.getColor(R.color.markerGray)) as Int)
                }
                .map {
                    BitmapDescriptorFactory.fromBitmap(it)
                }
                .toList()
    }


}