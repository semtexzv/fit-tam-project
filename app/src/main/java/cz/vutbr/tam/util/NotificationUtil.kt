package cz.vutbr.tam.util

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.os.Build
import android.support.v4.app.NotificationCompat
import android.text.format.DateUtils
import cz.vutbr.tam.App
import cz.vutbr.tam.R
import cz.vutbr.tam.data.model.Event
import cz.vutbr.tam.data.model.EventNotification
import cz.vutbr.tam.ui.main.MainActivity
import cz.vutbr.tam.ui.notify.NotifyDialogFragment
import org.threeten.bp.LocalDateTime
import org.threeten.bp.ZoneId
import timber.log.Timber

object NotificationUtil {

    val OptionTexts = listOf<String>(
            "Dont notify me",
            "5 minutes before",
            "1 hour before",
            "1 day before",
            "1 week before",
            "1 month before"
    )


    val OptionMinuteOffsets = listOf<Long>(
            0,
            5,
            60,
            60 * 24,
            60 * 24 * 7,
            60 * 24 * 7 * 4
    )

    val OptionMillisOffsets = OptionMinuteOffsets.map {
        it * 60 * 1000
    }


    val manager: NotificationManager by lazy {
        App.instance.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
    }

    val activeNotifs = mutableMapOf<Int, Notification>()
    var eventNotifs: Map<Int, EventNotification> = mapOf()


    var shouldHaveNotification = { notif: EventNotification ->
        val evTime = (notif.event?.startTime?.atZone(ZoneId.systemDefault())?.toEpochSecond() ?: Long.MAX_VALUE) * 1000

        val curTime = System.currentTimeMillis()
        val offTime = NotificationUtil.OptionMillisOffsets[notif.selectedOptionIndex]

        Timber.e("Time Diff: ev: ${evTime} cur: ${curTime} = ${evTime - curTime}")

        notif.selectedOptionIndex != 0 && evTime - curTime < offTime
    }

    fun recheckNotifications() {
        eventNotifs = App.realm.where(EventNotification::class.java).findAll().map {
            Pair(it.id, it)
        }.toMap()


        val toRemove = activeNotifs.filter {
            val en = eventNotifs[it.key]
            if (en != null) {
                !shouldHaveNotification(en)
            } else {
                true
            }
        }

        for (n in toRemove) {
            manager.cancel(n.key)
            activeNotifs.remove(n.key)
        }

        for (e in eventNotifs.values) {
            if (shouldHaveNotification(e)) {
                showNotification(e, e.event!!)
            }
        }
    }

    private fun showNotification(en: EventNotification, ev: Event) {
        if (Build.VERSION.SDK_INT >= 26) {
            val channel = NotificationChannel("main", "main", NotificationManager.IMPORTANCE_LOW)
            manager.createNotificationChannel(channel)
        }
        val i = App.instance.createActivityIntent(MainActivity::class.java, ev.id);
        val contentIntent = PendingIntent.getActivity(App.instance, 0, i, 0)

        val now = LocalDateTime.now().atZone(ZoneId.systemDefault()).toEpochSecond()
        val end = ev.startTime.atZone(ZoneId.systemDefault()).toEpochSecond()

        val txt = DateUtils.getRelativeTimeSpanString(end * 1000, now * 1000, DateUtils.MINUTE_IN_MILLIS);

        val notif = NotificationCompat.Builder(App.instance, "main")
                .setSmallIcon(R.drawable.clock)
                .setContentTitle(ev.name)
                .setContentText("Event starts " + txt)
                .setContentIntent(contentIntent)
                .setTicker("Aaa")
                .setWhen(end * 1000)

                .build()
        activeNotifs.put(en.id, notif)

        manager.notify(ev.id, notif)
    }
}