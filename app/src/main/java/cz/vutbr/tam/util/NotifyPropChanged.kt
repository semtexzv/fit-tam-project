package cz.vutbr.tam.util

import android.databinding.BaseObservable
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

class NotifyPropChanged<R : BaseObservable, T>(var value: T, vararg val bindingIds: Int = intArrayOf()) : ReadWriteProperty<R, T> {


    override fun getValue(thisRef: R, property: KProperty<*>): T {
        return value;
    }

    override fun setValue(thisRef: R, property: KProperty<*>, value: T) {
        this.value = value;
        if (bindingIds.isNotEmpty()) {
            for (x in bindingIds) {
                thisRef.notifyPropertyChanged(x)
            }
        } else {
            thisRef.notifyChange()
        }

    }
}

