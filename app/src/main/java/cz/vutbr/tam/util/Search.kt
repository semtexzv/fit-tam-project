package cz.vutbr.tam.util

import cz.vutbr.tam.App
import cz.vutbr.tam.data.model.Event
import cz.vutbr.tam.ui.filter.EventTimeFilter
import java.text.Normalizer

object Search {
    fun getEventsForQuery(events: List<Event>, q: String): List<Event> {
        val toFind = removeAccents(q);

        return events.filter {
            val orig = removeAccents(it.name)
            //val origContent = removeAccents(it.description)
            orig.contains(toFind) //|| origContent.contains(toFind)
        }
    }

    fun getEventsForFilter(events: List<Event>, filter: EventTimeFilter?): List<Event> {
        if (filter != null) {
            return events.filter {
                filter.eventMatches(it)
            }
        } else {
            return events
        }
    }

    fun removeAccents(word: String) = Normalizer
            .normalize(word, Normalizer.Form.NFD)
            .replace("[^\\p{ASCII}]".toRegex(), "")
            .toLowerCase()
}