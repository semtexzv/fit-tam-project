package cz.vutbr.tam.util

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.*
import android.os.Bundle
import android.os.Parcelable
import android.support.annotation.*
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import cz.vutbr.tam.ui.base.BaseActivity
import cz.vutbr.tam.ui.base.BaseFragment
import cz.vutbr.tam.ui.base.BaseViewHolder
import cz.vutbr.tam.ui.base.viewmodel.BaseViewModel
import cz.vutbr.tam.ui.base.viewmodel.MvvmViewModel
import org.parceler.Parcels
import android.text.TextUtils
import io.realm.*


fun <T : RecyclerView.ViewHolder> inflateViewHolder(viewGroup: ViewGroup, @LayoutRes layoutResId: Int, newViewHolderAction: (View) -> T): T {
    val view = LayoutInflater.from(viewGroup.context).inflate(layoutResId, viewGroup, false)
    return newViewHolderAction(view)
}

fun View.showKeyboard() {
    this.requestFocus()
    val inputMethodManager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.showSoftInput(this, InputMethodManager.SHOW_IMPLICIT)
}

fun View.hideKeyboard() {
    val inputMethodManager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.hideSoftInputFromWindow(windowToken, 0)
}

fun Context.hasPermission(perm: String): Boolean {
    return ContextCompat.checkSelfPermission(this, perm) == PackageManager.PERMISSION_GRANTED
}

val BaseViewModel<*>.context: Context?
    get() {
        if (this.view is BaseActivity<*, *>) {
            return this.view as BaseActivity<*, *>
        } else if (this.view is BaseFragment<*, *>) {
            return (this.view as BaseFragment<*, *>).activity as Context
        } else if (this is BaseViewHolder<*, *>) {
            return (this.view as BaseViewHolder<*, *>).itemView.context as Context
        } else {
            return null
        }
    }

val BaseViewModel<*>.activity: BaseActivity<*, *>?
    get() {
        if (this.view is BaseActivity<*, *>) {
            return this.view as BaseActivity<*, *>
        } else if (this.view is BaseFragment<*, *>) {
            return (this.view as BaseFragment<*, *>).activity as BaseActivity<*, *>
        } else if (this is BaseViewHolder<*, *>) {
            return (this.view as BaseViewHolder<*, *>).itemView.context as BaseActivity<*, *>
        } else {
            return null
        }
    }

fun propChangedCallback(method: (id: Int) -> Unit): android.databinding.Observable.OnPropertyChangedCallback {
    return object : android.databinding.Observable.OnPropertyChangedCallback() {
        override fun onPropertyChanged(sender: android.databinding.Observable?, propertyId: Int) {
            method(propertyId)
        }
    }
}

inline fun <reified T> Context.castWithUnwrap(): T? {
    if (this is T) {
        return this
    }
    var context = this
    while (context is ContextWrapper) {
        context = context.baseContext
        if (context is T) {
            return context
        }
    }
    return null
}

val EXTRA_ARG = "extra_arg"

fun <T> Context.createActivityIntent(clazz: Class<out Activity>, arg: T, flags: Int? = null): Intent {
    val intent = Intent(this, clazz)
    if (flags != null) {
        intent.setFlags(flags)
    }
    intent.putExtra(EXTRA_ARG, Parcels.wrap(arg))
    return intent
}

fun <T> Context.startActivityWithArg(clazz: Class<out Activity>, arg: T, flags: Int? = null) {
    val intent = createActivityIntent(clazz, arg, flags)

    startActivity(intent)
}

fun <T> Activity.getArg(i: Intent? = this.intent): T? {
    return Parcels.unwrap(i?.getParcelableExtra(EXTRA_ARG))
}

fun Activity.dimen(@DimenRes resId: Int): Int = resources.getDimension(resId).toInt()

fun Activity.color(@ColorRes resId: Int): Int = resources.getColor(resId)

fun Activity.integer(@IntegerRes resId: Int): Int = resources.getInteger(resId)

fun Activity.string(@StringRes resId: Int): String = resources.getString(resId)


val MILLISECONDS_PER_DAY: Long = 60 * 60 * 24 * 1000;
val MILLISECONDS_PER_WEEK: Long = MILLISECONDS_PER_DAY * 7;
val MILLISECONDS_PER_MONTH: Long = MILLISECONDS_PER_WEEK * 4;

fun Bitmap.tint(color: Int): Bitmap {
    val p = Paint()
    p.setColorFilter(PorterDuffColorFilter(color, PorterDuff.Mode.SRC_IN))
    val res = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
    val c = Canvas(res);
    c.drawBitmap(this, 0.0f, 0.0f, p);
    return res;
}

fun Bitmap.resize(w: Int, h: Int): Bitmap {
    return Bitmap.createScaledBitmap(this,w,h,true)
    /*
    val res = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888)
    val c = Canvas(res);
    c.drawBitmap(this, Rect(0, 0, width, height), Rect(0, 0, w, h), Paint());
    return res;*/
}